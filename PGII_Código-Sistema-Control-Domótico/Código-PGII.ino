#include <ESP8266WiFi.h>
#include "DHT.h"
#include "CTBot.h"

String ssid = "FARO A1";
String pass = "192.168.hello.world";
String token = "1344278873:AAHG5Q4x2bhV-pxo47muOR2rivDQsFtxSLw";
#include <MQTT.h>

#define D0   16 //GPIO16 - WAKE UP
#define D1   5  //GPIO5
#define D2   4  //GPIO4
#define D3   0  //GPIO0
#define D4   2  //GPIO2 - TXD1

#define D5   14 //GPIO14 - HSCLK
#define D6   12 //GPIO12 - HMISO
#define D7   13 //GPIO13 - HMOSI - RXD2
#define D8   15 //GPIO15 - HCS   - TXD2
#define RX   3  //GPIO3 - RXD0 
#define TX   1  //GPIO1 - TXD0

WiFiClient net;
MQTTClient client;
CTBot myBot;


int led = D4;
int foco = D5;
int toma = D6;
const byte  Obj_DHT11 = D7;
float h = 0;
float t = 0;
#define DHTPIN Obj_DHT11     // a qué pin digital está conectado el DHT11
#define DHTTYPE DHT11   // hay varios tipos de sensores DHT

String temp;
String hum;
DHT Obj_DHT(DHTPIN, DHTTYPE);

unsigned long lastMillis = 0;
TBMessage msg;

void connect() {
  Serial.print("\nConectando con MQTT...");
  while (!client.connect("IOTPG", "boludogt", "12345678")) {
    digitalWrite(led, 0);
    delay(1000);
    digitalWrite(led, 1);
    delay(1000);
    Serial.print("*");
  }

  Serial.println("\nConectado XD");
  client.subscribe("/telegram/casa");
  client.subscribe("/telegram/casa/temperatura");
}

void setup() {
  pinMode(led, OUTPUT);
  Serial.begin(115200);
  pinMode(toma, OUTPUT);
  pinMode(foco, OUTPUT);
  digitalWrite(toma, HIGH);
  digitalWrite(led, HIGH);
  digitalWrite(foco, HIGH);
  Obj_DHT.begin();


  Serial.println("\nIniciando TelegramBot ...");
  // conecte el ESP8266 al punto de acceso deseado
  myBot.wifiConnect(ssid, pass);

  // configurar el token del bot de telegrama
  myBot.setTelegramToken(token);

  // msj Bienvenida
  myBot.sendMessage(940459217, "\nBienvenido al control domótico de su residencia. \n\nPor favor digite las siguientes palabras (sin distinción entre mayúsculas y minúsculas) según lo que necesite verificar: \n\n • Encender luz / Apagar luz \n\n • Encender tomacorriente / Apagar tomacorriente \n\n • Temperatura \n\n • Humedad \n\n • Clima (muestra temperatura y humedad a la vez) \n");

  // comprueba si todo está bien
  if (myBot.testConnection())
    Serial.println("\nprueba de conexión, OK");

  else
    Serial.println("\nprueba de conexión, no OK");

  client.begin("broker.shiftr.io", net);
  client.onMessage(RecibirMensaje);
  connect();
}

void loop() {

  client.loop();
  delay(10);
  // una variable que almacena la información enviada a telegram
  TBMessage msg;

  // si hay un mensaje entrante ...
  if (myBot.getNewMessage(msg)) {
    Serial.println("\nRecibido: ");
    Serial.println(msg.text);

    // si hay un mensaje entrante con las palabras encender luz ...
    if (msg.text.equalsIgnoreCase("ENCENDER LUZ")) {
      digitalWrite(foco, LOW);
      myBot.sendMessage(msg.sender.id, "La luz se ha encendido");
      client.publish("/telegram/casa", "1");
      client.subscribe("/telegram/casa");
    }

    else if (msg.text.equalsIgnoreCase("APAGAR LUZ")) {
      digitalWrite(foco, HIGH);
      myBot.sendMessage(msg.sender.id, "La luz se ha apagado");
      client.publish("/telegram/casa", "2");
      client.subscribe("/telegram/casa");

    }

    // si hay un mensaje entrante con las palabras encender tomacorriente ...
    else if (msg.text.equalsIgnoreCase("ENCENDER TOMACORRIENTE")) {
      digitalWrite(toma, LOW);
      myBot.sendMessage(msg.sender.id, "El tomacorriente se ha encendido");
      client.publish("/telegram/casa", "3");
      client.subscribe("/telegram/casa");
    }

    else if (msg.text.equalsIgnoreCase("APAGAR TOMACORRIENTE")) {
      digitalWrite(toma, HIGH);
      myBot.sendMessage(msg.sender.id, "El tomacorriente se ha apagado");
      client.publish("/telegram/casa", "4");
      client.subscribe("/telegram/casa");
    }

    // si hay un mensaje entrante con la palabra temperatura ...
    else if (msg.text.equalsIgnoreCase("TEMPERATURA")) {
      float t = Obj_DHT.readTemperature();
      temp = t;
      String reply = (String)"Temperatura: " + (String)t + (String)" °C";
      myBot.sendMessage(msg.sender.id, reply);
      client.publish("/telegram/casa/temperatura", temp);
      Serial.print("Enviando lectura de temperatura:");
      Serial.println(temp);
      client.subscribe("/telegram/casa/temperatura");
    }

    // si hay un mensaje entrante con la palabra humedad ...
    else if (msg.text.equalsIgnoreCase("HUMEDAD")) {
      float h = Obj_DHT.readHumidity();
      hum = h;
      String reply2 = (String)"Humedad: " + (String)h + (String)" %";
      myBot.sendMessage(msg.sender.id, reply2);
      client.publish("/telegram/casa/humedad", hum);
      client.subscribe("/telegram/casa/temperatura");
      Serial.print("Enviando lectura de humedad:");
      Serial.println(hum);
    }

    // si hay un mensaje entrante con la palabra clima ...
    // muestra la temperatura y humedad a la vez
    else if (msg.text.equalsIgnoreCase("CLIMA")) {
      float t = Obj_DHT.readTemperature();
      temp = t;
      String reply = (String)"Temperatura: " + (String)t + (String)" °C   ";
      myBot.sendMessage(msg.sender.id, reply);
      client.publish("/telegram/casa/temperatura", temp);
      client.subscribe("/telegram/casa/temperatura");
      Serial.print("Enviando lectura de temperatura:");
      Serial.println(temp);

      
      float h = Obj_DHT.readHumidity();
      hum = h;
      String reply2 = (String)"Humedad: " + (String)h + (String)" %   ";
      myBot.sendMessage(msg.sender.id, reply2);
      client.publish("/telegram/casa/humedad", hum);
      client.subscribe("/telegram/casa/temperatura");
      Serial.print("Enviando lectura de humedad:");
      Serial.println(hum);
    }

    else {
      // genera el mensaje para el remitente por si se equivoca de palabra asignada
      String reply;
      reply = (String)"Bienvenido  " + msg.sender.username + (String)".  Comando no aceptado, favor corrobore la orden.";
      myBot.sendMessage(msg.sender.id, reply);             // and send it
    }
  }

  if (!client.connected()) {
    connect();
  }

}

void RecibirMensaje(String &topic, String &payload) {
  Serial.println("Mensaje: " + topic + "  " + payload);
}
